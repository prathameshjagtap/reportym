$(document).ready(function(){

    $("button#add_employer").click(function(){
        $.ajax({
        	url: "employee_request/" + $("#employer_email").val(),
        	success:function(result){
        		alert(result);
        	},
        	error:function (xhr, ajaxOptions, thrownError){
        	    if(xhr.status==404) {
        	        alert("Employer not found with email : " + $("#employer_email").val());
        	    }
        	}
        });
    });
    
    
});