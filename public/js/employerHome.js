$(document).ready(function(){

    $("button#add_employee").click(function(){
        $.ajax({
        	url: "employer_request/" + $("#employee_email").val(),
        	success:function(result){
        		alert(result);
        	},
        	error:function (xhr, ajaxOptions, thrownError){
        	    if(xhr.status==404) {
        	        alert("Employee not found with email : " + $("#employee_email").val());
        	    }
        	}
        });
    });
    
    
});