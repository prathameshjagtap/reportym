$(document).ready(function(){

	rebind();

});

function rebind(){
	
	$("#next").click(function(){
        $.ajax({
			url:"timesheet.php",
			data:{
				monthToShow: parseInt($("#monthValue").val()) + 1, 
				yearToShow : $("#yearValue").val()
			},
			method:"GET",
			success:function(result){
				$("#calender_container").html(result);
				rebind();
			}
		});
    });
	
    $("#prev").click(function(){
        $.ajax({
			url:"timesheet.php",
			data:{
				monthToShow: parseInt($("#monthValue").val()) - 1, 
				yearToShow : $("#yearValue").val()
			},
			method:"GET",
			success:function(result){
				$("#calender_container").html(result);
				rebind();
			}
		});
    });


}