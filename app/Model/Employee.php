<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Employee extends Eloquent{
	
	protected $fillable = array('employee_id','rate', 'password', 'status_code', 'ip_address');
	
	protected  $table = "t_employee_account";
	protected  $primaryKey = "employee_id";
	
}

?>