<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Person extends Eloquent{
	
	protected $fillable = array('person_email', 'phone_no', 'person_name');
	
	protected  $table = "t_person";
	protected  $primaryKey = "person_id";
	
}

?>