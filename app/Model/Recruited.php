<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Recruited extends Eloquent{
	
	protected  $table = "t_recruited";
	protected  $primaryKey = "recruited_id";
	
}
	
	?>