<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Employer extends Eloquent{

	protected $fillable = array('employer_id','contact_person_id', 'location', 'password', 'status_code', 'ip_address');
	
	protected  $table = "t_employer_account";
	protected  $primaryKey = "EMPLOYER_ID";
	
}

?>