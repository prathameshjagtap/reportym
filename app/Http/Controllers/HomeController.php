<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\Recruited_v;

class HomeController extends Controller
{
    function employeeHome(Request $request) {
    	
    	$employee = Session::get('Employee');
    	
    	if (!$employee) {
    		return redirect('employee_login');
    	}
    	
    	$employers = Recruited_v::where('employee_id', $employee->employee_id)->get();
    	
    	return view('employee_home', compact('employee','employers'));
    }
    
    function employerHome(Request $request) {
    	 
    	$employer = Session::get('Employer');
    	 
    	if (!$employer) {
    		return redirect('employer_login');
    	}
    	 
    	$employees = Recruited_v::where('employer_id', $employer->employer_id)->get();
    	 
    	return view('employerHome', compact('employer','employees'));
    }
    
    
}
