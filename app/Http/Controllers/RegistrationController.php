<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Person;
use App\Model\Employee;
use App\Model\Employer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\EmployeeSignupRequest;
use App\Http\Requests\EmployerSignupRequest;

class RegistrationController extends Controller {

	function registerEmployee(EmployeeSignupRequest $request) {
		
			DB::transaction (function () use ($request){
				$person = Person::create ($request->all());
				
				$emp_array = $request->all ();
				$emp_array = array_add ( $emp_array, 'employee_id', $person->person_id );
				$emp_array = array_add ( $emp_array, 'ip_address', $_SERVER ['REMOTE_ADDR'] );
				$emp_array = array_add ( $emp_array, 'status_code', 'ACT' );
				
				$emp = Employee::create ( $emp_array );
			} );
			return redirect('employee_signup');
	}
	
	function registerEmployer(EmployerSignupRequest $request) {

		DB::transaction (function () use ($request){
			$person = Person::create ($request->all());
		
			$emp_array = $request->all ();
				
			if ($request->contact_person_email != null) {
				$contact_person = new Person();
				$contact_person->person_name = $request->contact_person_name;
				$contact_person->person_email = $request->contact_person_email;
				$contact_person->phone_no = $request->phone_no;
				$contact_person->save();
				$emp_array = array_add ( $emp_array, 'contact_person_id', $contact_person->person_id );
			}
			
			$emp_array = array_add ( $emp_array, 'employer_id', $person->person_id );
			$emp_array = array_add ( $emp_array, 'ip_address', $_SERVER ['REMOTE_ADDR'] );
			$emp_array = array_add ( $emp_array, 'status_code', 'ACT' );
		
			$emp = Employer::create ( $emp_array );
		});

		return redirect('employer_signup');
	}
}
