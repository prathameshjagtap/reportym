<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeLoginRequest;
use App\Model\Employee_v;
use App\Model\Employer_v;
use App\Http\Requests\EmployerLoginRequest;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    function employeeLogin(EmployeeLoginRequest $request) {
    	$employee = Employee_v::where('person_email',$request->person_email)->where('password',$request->password)->first();
    	if($employee){
			Session::put('Employee', $employee);
    		return redirect('employee_home');
    	} else {
    		return view ('employee_login', ['message'=>'Invalid credentials']);
    	}
    }
	
    function employerLogin(EmployerLoginRequest $request) {
    	$employer = Employer_v::where('person_email',$request->person_email)->where('password',$request->password)->first();
    	if($employer){
    		Session::put('Employer', $employer);
    		return redirect('employerHome');
    	} else{
    		return view ('employer_login', ['message'=>'Invalid credentials']);
    	}
    }    
}
