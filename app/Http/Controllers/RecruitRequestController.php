<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Employer_v;
use App\Model\Recruited;
use Illuminate\Support\Facades\Session;

class RecruitRequestController extends Controller
{
    function employeeRequest(Employer_v $employer) {
		$employee = Session::get('Employee');
		
		$recruited = new Recruited();
		$recruited->employee_id = $employee->employee_id;
		$recruited->employer_id = $employer->employer_id;
		$recruited->requester = "EPE";
		$recruited->approval_status = "PEND";
		$recruited->save();
		
		return "Employer Requested ($employer->person_name)";
    }

    function employerRequest(Employee_v $employee) {
		$employer = Session::get('Employer');
		
		$recruited = new Recruited();
		$recruited->employee_id = $employee->employee_id;
		$recruited->employer_id = $employer->employer_id;
		$recruited->requester = "EPR";
		$recruited->approval_status = "PEND";
		$recruited->save();
		
		return "Employee Requested ($employee->person_name)";
    }
    
    function employerAccept(Recruited $recruited){
    	$recruited->approval_status = "APP";
    	$recruited->save();
    	return redirect('employerHome');
    	
    }
    
    function employerReject(Recruited $recruited){
    	$recruited->approval_status = "REJ";
    	$recruited->save();
    	return redirect('employerHome');
    }
    
    function employeeAccept(Recruited $recruited){
    	$recruited->approval_status = "APP";
    	$recruited->save();
    	return redirect('employee_home');
    	 
    }
    
    function employeeReject(Recruited $recruited){
    	$recruited->approval_status = "REJ";
    	$recruited->save();
    	return redirect('employee_home');
    }
    
}
