<?php

namespace App\Http\Controllers;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    function employeeLogout(){
    	Session::put('Employee',null);

		return view('employee_login');
	}
	
	function employerLogout(){
		Session::put('Employer',null);
	
		return view('employer_login');
	}
}
