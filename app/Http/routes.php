<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::model('employee_v','App\Model\Employee_v');
Route::model('employer_v','App\Model\Employer_v');
Route::model('recruited','App\Model\Recruited');

Route::get('/', function () {
    return view('welcome');
});

Route::get('employee_signup', function (){
	return view('employee_signup');
});

Route::get('employer_signup', function (){
	return view('employer_signup');
});

Route::get('employee_login',function (){
	return view('employee_login');
});

Route::get('employer_login',function (){
	return view('employer_login');
});

Route::get('employee_home', 'HomeController@employeeHome');
Route::get('employerHome', 'HomeController@employerHome');

Route::get('employee_timesheet','TimesheetController@employeeTimesheet');
Route::get('employer_timesheet','TimesheetController@employerTimesheet');

Route::post('employee_signup', 'RegistrationController@registerEmployee');
Route::post('employer_signup', 'RegistrationController@registerEmployer');

Route::post('employee_login', 'LoginController@employeeLogin');
Route::post('employer_login', 'LoginController@employerLogin');

Route::any('employee_logout', 'LogoutController@employeeLogout');
Route::any('employer_logout', 'LogoutController@employerLogout');

Route::any('employee_notification','NotificationController@employeeNotification');
Route::any('employer_notification','NotificationController@employerNotification');

Route::get('employee_request/{employer_v}', 'RecruitRequestController@employeeRequest');
Route::get('employer_request/{employee_v}', 'RecruitRequestController@employerRequest');
Route::get('employer_accept/{recruited}', 'RecruitRequestController@employerAccept');
Route::get('employer_reject/{recruited}', 'RecruitRequestController@employerReject');
Route::get('employee_accept/{recruited}', 'RecruitRequestController@employeeAccept');
Route::get('employee_reject/{recruited}', 'RecruitRequestController@employeeReject');
