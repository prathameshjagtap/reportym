<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/mystyle.css">
<link rel="stylesheet" type="text/css" href="css/calendar.css">
<style type="text/css">
table {
	width: 600px;
	border: 0px solid #888;
	border-collapse: collapse;
}

th {
	font-family: Arial, sans-serif;
	border-collapse: collapse;
	border: 1px solid #888;
	background-color: #E9ECEF;
	text-align: center;
	height: 40px;
}

td {
	width: 27px;
	font-family: Arial, sans-serif;
	border-collapse: collapse;
	border: 1px solid #888;
	vertical-align: top text-align: left;
	height: 60px;
}

.calblock {
	float: none;
}

.onecal {
	vertical-align: top;
	background-color: #E9ECEF;
}

.cal {
	border-style: solid;
	border-width: thin;
	border-color: #E9ECEF;
	float: left;
	padding: 3px;
}

.days {
	background-color: #F1F3F5;
}

.hasday {
	background-color: #FFFFDE;
}

.noday {
	background-color: #E9ECEF;
}

input[type="text"] {
	width: 50%;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
}
</style>

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/employee_home.js"></script>
<script src="js/ajax_employee_accept.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>

			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/employee_home">Home</a></li>
					<li><a href="/employee_timesheet">Upload Timesheet</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="employee_notification"><span
							class="glyphicon glyphicon-bell"></span>Notifications</a></li>
					<li><a href="/employee_logout"><span
							class="glyphicon glyphicon-log-out"></span> Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<h1>WELCOME {{ $employee->person_name }}</h1>

	<table>
		<tr>
			<th>EMPLOYER</th>
			<th>EMAIL</th>
			<th>REQUEST DATE</th>
			<th>REQUESTER</th>
			<th>Status</th>
		</tr>
		
		@foreach ($employers as $employer)
		<tr>
			<td>{{ $employer->employer_name }}</td>
			<td>{{ $employer->employer_email }}</td>
			<td>{{ $employer->updated_at }}</td>
			<td>{{ $employer->requester }}</td>
			<td>
			@if($employer->approval_status == "PEND")
				@if($employer->requester == "EPE") 
					PENDING
					@else
						<span style="color: #7FFF00; cursor: pointer; text-align: center" value="" class="accept"><a href="employee_accept/{{$employer->recruited_id}}">ACCEPT</a></span> 
						<span style="color: red; cursor: pointer; text-align: center" value="" class="reject"><a href="employee_reject/{{$employer->recruited_id}}">REJECT</a></span> 
				@endif
			
			@elseif($employer->approval_status == "REJ")
				REJECTED
			@else
				APPROVED
			@endif
				

			</td>
		</tr>
		@endforeach
	</table>

	<h3>Add Employer</h3>
	<input type="email" id="employer_email" name="email"
		placeholder="Enter employer email">
	<button id="add_employer">Add EMPLOYER</button>
	<hr />
	<form action="employee_add_approver.php" method="post">

		<?php //include 'timesheet.php'; ?>

<h3>Add APPROVER</h3>
		<input type="text" id="approver_name" name="approver_name"
			placeholder="Enter approver name"> <input type="email"
			id="approver_email" name="approver_email"
			placeholder="Enter approver email">
		<button id="add_approver">Add APPROVER</button>

	</form>