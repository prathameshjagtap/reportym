<!DOCTYPE html>
<html lang="en">
<head>
  <title>Organization Signup</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<body>

<h2>Organization Signup</h2>

	@if($errors->has())
		@foreach($errors->all() as $error ) 
			{{ $error }}
		@endforeach
	@endif
	
<div class="container">
	
	{!! Form::open(
		array(
		'action' => 'RegistrationController@registerEmployer', 
		'class' => 'form-horizontal'
		)) !!}

	<div class="form-group">
      <label class="control-label col-sm-2" for="name">Organization Name:</label>
      <div class="col-sm-8">
      	{!! Form::text('person_name', null, array('placeholder'=>'Enter name','class'=>'form-control'))!!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-8">
      	{!! Form::email('person_email', null, array('placeholder'=>'Enter email','class'=>'form-control'))!!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-8">          
      	{!! Form::password('password', array('placeholder'=>'Enter password','class'=>'form-control'))!!}
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2" for="cnt_name">Contact Person Name:</label>
      <div class="col-sm-8">
      	{!! Form::text('contact_person_name', null, array('placeholder'=>'Enter contact person name','class'=>'form-control'))!!}
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2" for="phone">Contact Person Email:</label>
      <div class="col-sm-8">
      	{!! Form::email('contact_person_email', null, array('placeholder'=>'Enter contact person email','class'=>'form-control'))!!}
      </div>
    </div>
   	<div class="form-group">
      <label class="control-label col-sm-2" for="phone">Phone Number:</label>
  	    <div class="col-sm-8">          
      	{!! Form::text('phone_no', null, array('placeholder'=>'Enter contact phone number','class'=>'form-control'))!!}
      </div>
    </div>
   	<div class="form-group">
      <label class="control-label col-sm-2" for="location">Location:</label>
      <div class="col-sm-8">
      	{!! Form::text('location', null, array('placeholder'=>'Enter location','class'=>'form-control'))!!}
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label><input type="checkbox"> Remember me</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
      	{!! Form::submit('Sign up!', array('class'=>'btn btn-default')) !!}
      </div>
    </div>
    {!! Form::close() !!}
</div>

</body>
</html>


