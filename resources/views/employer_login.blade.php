<!DOCTYPE html>
<html lang="en">
<head>
  <title>Employer Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

<h2>Employer Login</h2>

<div class="container">
@if(isset($message))
	{{ $message }}
@endif

@if($errors->has())
		@foreach($errors->all() as $error ) 
			{{ $error }}
		@endforeach
	@endif

	{!! Form::open(
		array(
		'action' => 'LoginController@employerLogin', 
		'class' => 'form-horizontal'
		)) !!}

   
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-8">
      {!! Form::text('person_email', null, array('placeholder'=>'Enter email','class'=>'form-control'))!!}
      
      </div>
    </div>
    <div class="form-group">
    
      <label class="control-label col-sm-2" for="password">Password:</label>
      <div class="col-sm-8">
     		{!! Form::password('password', array('placeholder'=>'Enter password','class'=>'form-control'))!!}          
        
      </div>
    </div>
	
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
       		{!! Form::submit('Login!', array('class'=>'btn btn-default')) !!}
       	
      </div>
    </div>
	
  	{!! Form::close() !!}
</div>

</body>
</html>


