<!DOCTYPE html>
<html lang="en">
<head>
  <title>Employee Signup</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<body>
<h2> Employee Signup</h2>
<div class="container">
	@if($errors->has())
		@foreach($errors->all() as $error ) 
			{{ $error }}
		@endforeach
	@endif

	{!! Form::open(
		array(
		'action' => 'RegistrationController@registerEmployee', 
		'class' => 'form-horizontal'
		)) !!}

	<div class="form-group">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-8">
      	{!! Form::text('person_name', null, array('placeholder'=>'Enter name','class'=>'form-control'))!!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-8">
      	{!! Form::email('person_email', null, array('placeholder'=>'Enter email','class'=>'form-control'))!!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-8">
      {!! Form::password('password', array('placeholder'=>'Enter password','class'=>'form-control'))!!}
                
     <!--   <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password"> -->
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2" for="phone">Phone Number:</label>
      <div class="col-sm-8">          
        {!! Form::text('phone_no', null, array('placeholder'=>'Enter phone number','class'=>'form-control'))!!}
        <!--<input type="text" class="form-control" id="phone" name="phone_no" placeholder="Enter phone number">-->
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2" for="rate">Rate (Optional):</label>
      <div class="col-sm-8">
        {!! Form::text('rate', null, array('placeholder'=>'Enter rate','class'=>'form-control')) !!}
        <!--<input type="text" class="form-control" id="rate" name="rate" placeholder="Enter rate">-->
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label><input type="checkbox"> Remember me</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
	{!! Form::close() !!}
</div>

</body>
</html>


